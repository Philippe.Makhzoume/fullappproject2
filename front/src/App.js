import React, { Component } from "react";
import { withRouter, Route, Switch, Link } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { pause, makeRequestUrl } from "./utils.js";
import "./App.css";
/* AUTH */
import * as auth0Client from "./auth";
import IfAuthenticated from './IfAuthenticated';
import SecuredRoute from './SecuredRoute';
/* COMPONENTS */
import ItemList from "./ItemList";
import Item from "./item";

const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

class App extends Component {
  state = {
    items_list: [],
    error_message: "",
    name: "",
    type: "",
    color: "",
    taste: "",
    isLoading: false,
    checkingSession: true
  };
  async componentDidMount() {
    this.getItemsList();
    if (this.props.location.pathname === "/callback") {
      this.setState({ checkingSession: false });
      return;
    }
    try {
      await auth0Client.silentAuth();
      await this.getPersonalPageData(); // get the data from our server
    } catch (err) {
      if (err.error !== "login_required") {
        console.log(err.error);
      }
    }
    this.setState({ checkingSession: false });
  }
  getItem = async id => {
    // check if we already have the item
    const previous_item = this.state.items_list.find(
      item => item.id === id
    );
    if (previous_item) {
      return; // do nothing, no need to reload a item we already have
    }
    try {
      const url = makeUrl(`items/get/${id}`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of items
        const item = answer.result;
        const items_list = [...this.state.items_list, item];
        this.setState({ items_list });
        toast(`item loaded`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  deleteItem = async id => {
    try {
      const url = makeUrl(`items/delete/${id}`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const items_list = this.state.items_list.filter(
          item => item.id !== id
        );
        this.setState({ items_list });
        toast(`item deleted`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  updateItem = async (id, props) => {
    try {
      if (!props || !(props.name || props.type ||  props.color || props.taste)) {
        throw new Error(
          `you need at least name or type, or color, taste properties to update a item`
        );
      }
      const url = makeUrl(`items/update/${id}`, {
        name: props.name,
        type: props.type,
        color: props.color,
        taste: props.taste
      });

      let body = null;

      if(props.image)
      {
        body = new FormData();
        body.append(`image`, props.image)
      }

      const response = await fetch(url, {
        method:'POST', 
        body,
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const items_list = this.state.items_list.map(item => {
          // if this is the item we need to change, update it. This will apply to exactly
          // one item
          if (item.id === id) {
            const new_item = {
              id: item.id,
              name: props.name || item.name,
              type: props.type || item.type,
              color: props.color || item.color,
              taste: props.taste || item.taste
            };
            toast(`item "${new_item.name}" updated`);
            return new_item;
          }
          // otherwise, don't change the item at all
          else {
            return item;
          }
        });
        this.setState({ items_list });
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };
  createItem = async props => {
    try {
      if (!props || !(props.name && props.type && props.color && props.taste)) {
        throw new Error(
          `you need both name,type,color,taste properties to create a item`
        );
      }
      const { name, type, color, taste, image } = props;
      const url = makeUrl(`items/new`, {
        name,
        type,
        color,
        taste
      });
      
      let body = null;
      if(image)
      {
        body = new FormData();
        body.append(`image`, image)
      }

      const response = await fetch(url, {
        method:'POST', 
        body,
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const item = { name, type, color, taste, id };
        const items_list = [...this.state.items_list, item];
        this.setState({ items_list });
        toast(`item "${name}" created`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  getItemsList = async order => {
    this.setState({ isLoading: true });
    try {
      const url = makeUrl(`items/list`, { order });
      const response = await fetch(url);
      await pause();
      const answer = await response.json();
      if (answer.success) {
        const items_list = answer.result;
        this.setState({ items_list, isLoading: false });
        toast("items loaded");
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
      toast.error(err.message);
    }
  };
  getPersonalPageData = async () => {
    try {
      const url = makeUrl(`mypage`);
      const response = await fetch(url, {
        headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
      });
      const answer = await response.json();
      if (answer.success) {
        const user = answer.result;
        this.setState({ user });
        if (user.firstTime) {
          toast(`welcome ${user.nickname}! We hope you'll like it here'`);
        }
        toast(`hello ${user.nickname}'`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(
          `error message received from the server: ${answer.message}`
        );
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };
  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    // extract name and type, color, taste from state
    const { name, type, color, taste } = this.state;
    // get the files
    const image = evt.target.item_image_input.files[0]
    // create the item from name, type, color, taste, image
    this.createItem({ name, type, color, taste, image });
    // empty name and type,color,taste so the text input fields are reset
    this.setState({ name: "", type: "", color: "", taste: "" });
    this.props.history.push("/");
  };
  renderUser() {
    const isLoggedIn = auth0Client.isAuthenticated();
    if (isLoggedIn) {
      // user is logged in
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  renderUserLoggedOut() {
    return <button onClick={auth0Client.signIn}>Sign In</button>;
  }
  renderUserLoggedIn() {
    const nick = auth0Client.getProfile().name;
    const user_items = this.state.user.items.map(itemFromUser =>
      this.state.items_list.find(
        itemFromMain => itemFromMain.id === itemFromUser.id
      )
    );
    return (
      <div>
        Hello, {nick}!{" "}
        <button
          onClick={() => {
            auth0Client.signOut();
            this.setState({});
          }}
        >
          logout
        </button>
        <div>
          <ItemList items_list={user_items} />
        </div>
      </div>
    );
  }
  renderHomePage = () => {
    const { items_list } = this.state;
    return <ItemList items_list={items_list} />;
  };
  renderItemPage = ({ match }) => {
    const id = match.params.id;
    // find the item:
    // eslint-disable-next-line eqeqeq
    const item = this.state.items_list.find(item => item.id == id);
    // we use double equality because our ids are numbers, and the id provided by the router is a string
    if (!item) {
      return <div>{id} not found</div>;
    }
    return (
      <Item
        id={item.id}
        name={item.name}
        type={item.type}
        color={item.color}
        taste={item.taste}
        author_id={item.author_id}
        image={item.image}
        updateItem={this.updateItem}
        deleteItem={this.deleteItem}
      />
    );
  };
  renderProfilePage = () => {
    if (this.state.checkingSession) {
      return <p>validating session...</p>;
    }
    return (
      <div>
        <p>profile page</p>
        {this.renderUser()}
      </div>
    );
  };
  renderCreateForm = () => {
    return (
      <form className="third" onSubmit={this.onSubmit}>
        <input
          type="text"
          placeholder="name"
          onChange={evt => this.setState({ name: evt.target.value })}
          value={this.state.name}
        />
        <input
          type="text"
          placeholder="type"
          onChange={evt => this.setState({ type: evt.target.value })}
          value={this.state.type}
        />
        <input
          type="text"
          placeholder="color"
          onChange={evt => this.setState({ color: evt.target.value })}
          value={this.state.color}
        />
        <input
          type="text"
          placeholder="taste"
          onChange={evt => this.setState({ taste: evt.target.value })}
          value={this.state.taste}
        />
        <input
          type="file"
          name="item_image_input"
        />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
    );
  };
  renderContent() {
    if (this.state.isLoading) {
      return <p>loading...</p>;
    }
    return (
      <Switch>
        <Route path="/" exact render={this.renderHomePage} />
        <Route path="/item/:id" render={this.renderItemPage} />
        <Route path="/profile" render={this.renderProfilePage} />
        <SecuredRoute path="/create" render={this.renderCreateForm} />
        <Route path="/callback" render={this.handleAuthentication} />
        <Route render={() => <div>not found!</div>} />
      </Switch>
    );
  }
  isLogging = false;
  login = async () => {
    if (this.isLogging === true) { 
      return;
    }
    this.isLogging = true;
    try {
      await auth0Client.handleAuthentication();
      await this.getPersonalPageData(); // get the data from our server
      this.props.history.push("/profile");
    } catch (err) {
      this.isLogging = false;
      toast.error(`error from the server: ${err.message}`);
    }
  };
  handleAuthentication = () => {
    this.login();
    return <p>wait...</p>;
  };
  render() {
    return (
      <div className="App">
        <div>
          <Link to="/">Home</Link> |
          <Link to="/profile">profile</Link> |
          <IfAuthenticated>
            <Link to="/create">create</Link>
          </IfAuthenticated>
        </div>
        {this.renderContent()}
        <ToastContainer />
      </div>
    );
  }
}

export default withRouter(App);
