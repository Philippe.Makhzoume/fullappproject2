import React from "react";
import { Transition } from "react-spring"; // you can remove this from App.js
import { Link } from "react-router-dom";

const ItemList = ({ items_list }) => (
  <Transition
    items={items_list}
    keys={item => item.id}
    from={{ transform: "translate3d(-100px,0,0)" }}
    enter={{ transform: "translate3d(0,0px,0)" }}
    leave={{ transform: "translate3d(-100px,0,0)" }}
  >
    { item => style => (
      <div style={style}>
        <Link to={"/item/"+item.id}>
        {item.name}
        </Link>
      </div>
    )}
  </Transition>
);

export default ItemList
