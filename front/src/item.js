import React from 'react';
import * as auth0Client from "./auth";

export default class Item extends React.Component{
    state = {
      editMode:false
    }
    toggleEditMode = () => {
      const editMode = !this.state.editMode
      this.setState({editMode})
    }
    renderEditMode(){
        const { name, type, color, taste } = this.props
        return(
        <form className="third" onSubmit={this.onSubmit} onReset={this.toggleEditMode}>
          <input
            type="text"
            placeholder="name"
            name="item_name_input"
            defaultValue={name}
          />
          <input
            type="text"
            placeholder="type"
            name="item_type_input"
            defaultValue={type}
          />
          <input
            type="text"
            placeholder="color"
            name="item_color_input"
            defaultValue={color}
          />
          <input
            type="text"
            placeholder="taste"
            name="item_taste_input"
            defaultValue={taste}
          />
		  <input
          type="file"
          name="contact_image_input"
		  />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
        )
      }
      onSubmit = (evt) => {
        // stop the page from refreshing
        evt.preventDefault()
        // target the form
        const form = evt.target
        // extract the two inputs from the form
        const item_name_input = form.item_name_input 
        const item_type_input = form.item_type_input 
        const item_color_input = form.item_name_input 
        const item_taste_input = form.item_type_input 
		const contact_image_input = form.contact_image_input;
        // extract the values
        const name = item_name_input.value
        const type = item_type_input.value
        const color = item_color_input.value
        const taste = item_taste_input.value
		const image = contact_image_input.files[0];
        // get the id and the update function from the props
        const { id, updateItem } = this.props
        // run the update item function
        updateItem(id,{ name, type, color, taste, image })
        // toggle back view mode
        this.toggleEditMode()
      }
    
      renderViewMode() {
        const { id, name, author_id, deleteItem, image } = this.props;
        const isLoggedIn = auth0Client.isAuthenticated();
        const current_logged_in_user_id = isLoggedIn && auth0Client.getProfile().sub
        const is_author = author_id === current_logged_in_user_id;
        return (
          <div>
			{ image && <img src={`//localhost:8080/images/${image}`} alt={`the avatar of ${name}`}/> }
            <span>
              {id} - {name}
            </span>
            { is_author && isLoggedIn ?
              <div>
                  <button onClick={this.toggleEditMode} className="success">
                    edit
                  </button>
                  <button onClick={() => deleteItem(id)} className="warning">
                    x
                  </button>
              </div>
            : false
            }
          </div>
        );
    }    
    render(){
      const { editMode } = this.state
      if(editMode){
        return this.renderEditMode()
      }
      else{
        return this.renderViewMode()
      }
    }
  }
  