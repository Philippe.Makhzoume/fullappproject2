import app from './app'
import initializeDatabase from './db'
import { authenticateUser, logout, isLoggedIn } from './auth'
import path from 'path'
import multer from 'multer' 

const multerStorage = multer.diskStorage({
  destination: path.join( __dirname, '../public/images'),
  filename: (req, file, cb) => {
    const { fieldname, originalname } = file
    const date = Date.now()
    // filename will be: image-1345923023436343-filename.png
    const filename = `${fieldname}-${date}-${originalname}` 
    cb(null, filename)
  }
})
const upload = multer({ storage: multerStorage  })

const start = async () => {
  const controller = await initializeDatabase()

  app.get('/', (req, res, next) => res.send("ok"));

  // CREATE
  app.post("/items/new", isLoggedIn, upload.single('image'), async (req, res, next) => {
    const author_id = req.user.sub
    try {
      const {
        name,
        type,
        color,
        taste
      } = req.query;
      const image = req.file && req.file.filename
      const result = await controller.createItem({
        name,
        type,
        color,
        taste,
        image,
        author_id
      });
      res.json({
        success: true,
        result
      });
    } catch (e) {
      next(e);
    }
  });


  // READ
  app.get('/items/get/:id', async (req, res, next) => {
    try {
      const {
        id
      } = req.params
      const item = await controller.getItem(id)
      res.json({
        success: true,
        result: item
      })
    } catch (e) {
      next(e)
    }
  })

  // DELETE
  app.get("/items/delete/:id", isLoggedIn, async (req, res, next) => {
    const author_id = req.user.sub
    try {
      const { id } = req.params;
      const result = await controller.deleteItem({id, author_id});
      res.json({
        success: true,
        result
      })
    } catch (e) {
      next(e)
    }
  })

  // UPDATE
  app.post("/items/update/:id", isLoggedIn, upload.single('image'), async (req, res, next) => {
    const author_id = req.user.sub
    try {
      const { id } = req.params;
      const { name, type, color, taste } = req.query;
      const image = req.file && req.file.filename
      const result = await controller.updateItem(id, { name, type, color, taste, author_id, image });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });


  // LIST
  app.get("/items/list", async (req, res, next) => {
    try {
      const { order, desc } = req.query;
      const items = await controller.getItemsList({order, desc});
      res.json({ success: true, result: items });
    } catch (e) {
      next(e);
    }
  });

  app.get('/mypage', isLoggedIn, async ( req, res, next ) => {
    try{
      const { order, desc } = req.query;
      const { sub, nickname} = req.user
      const user = await controller.createUserIfNotExists({sub, nickname})
      const items = await controller.getItemsList({order, desc, author_id:sub})
      user.items = items
      res.json({ success: true, result: user });
    }catch(e){
      next(e)
    }
  })
  
  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({
      success: false,
      message
    })
  })

  app.listen(8080, () => console.log('server listening on port 8080'))
}

start();
