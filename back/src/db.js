import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const nowForSQLite = () => new Date().toISOString().replace('T',' ').replace('Z','');

const joinSQLStatementKeys = (keys, values, delimiter , keyValueSeparator='=') => {
  return keys
    .map(propName => {
      const value = values[propName];
      if (value !== null && typeof value !== "undefined") {
        return SQL``.append(propName).append(keyValueSeparator).append(SQL`${value}`);
      }
      return false;
    })
    .filter(Boolean)
    .reduce((prev, curr) => prev.append(delimiter).append(curr));
};

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  const createItem = async (props) => {
    if (!props || 
      !props.name || 
      !props.type || 
      !props.color || 
      !props.taste || 
      !props.author_id || 
      !props.image) {
      throw new Error(`you must provide a name, a color, a taste, and an author_id`);
    }

    const { name, type, color,taste, author_id, image } = props;
    const date = nowForSQLite();
    try {
      const result = await db.run(
        SQL`INSERT INTO items (name,type,color,taste, date, image, author_id) VALUES 
        (${name}, ${type}, ${color}, ${taste}, ${date}, ${image}, ${author_id})`
      );
      const id = result.stmt.lastID
      return id
    }catch(e){
      throw new Error(`couldn't insert this combination: `+e.message)
    }
  }

  const deleteItem = async (props) => {
    const { id, author_id } = props
    try {
      const result = await db.run(
        SQL`DELETE FROM items WHERE item_id = ${id} AND author_id = ${author_id}`
      );
      if (result.stmt.changes === 0) {
        throw new Error(`item "${id}" does not exist or wrong author_id`);
      }
      return true
    }catch(e){
      throw new Error(`couldn't delete the item "${id}": `+e.message)
    }
  }
  
  const updateItem = async (item_id, props) => {
    if (
      (!props || !(props.name ||  props.type ||   props.color ||  props.taste || props.image), !props.author_id)
    ) {
      throw new Error(
        `you must provide a name, or type, or color, or taste, or image, and an author_id`
      );
    }
    try {
      const previousProps = await getItem(item_id)
      const newProps = {...previousProps, ...props }
      const statement = SQL`UPDATE items SET `
        .append(
          joinSQLStatementKeys(
            ["name", "type", "color", "taste", "image"],
            newProps,
            ", "
          )
        )
        .append(SQL` WHERE `)
        .append(
          joinSQLStatementKeys(
            ["item_id", "author_id"],
            { item_id:item_id, author_id:props.author_id },
            " AND "
          )
        );
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes were made`);
      }
      return true;
    } catch (e) {
      throw new Error(`couldn't update the item ${item_id}: ` + e.message);
    }
  };

  const getItem = async id => {
    try {
      const itemsList = await db.all(
        SQL`SELECT item_id AS id, name, type, color, taste, image, author_id FROM items WHERE item_id = ${id}`
      );
      const item = itemsList[0]
      if(!item){
        throw new Error(`item ${id} not found`)
      }
      return item
    }catch(e){
      throw new Error(`couldn't get the item ${id}: `+e.message)
    }
  }
  
  const getItemsList = async props => {
    const { orderBy, author_id, desc, limit, start } = props;
    const orderProperty = /name|type|color|taste|date|item_id/.test(orderBy)
      ? orderBy
      : "item_id";
    const startingId = start 
      ? start // if start is provided, use that
      : orderProperty === "item_id" // otherwise, if we're order by `item_id`:
      ? 0 // default `startingId` is 0 
      : orderProperty === "date" // otherwise, if we're ordering by `date`
      ? "1970-01-01 00:00:00.000" // default property is an old date
      : "a"; // otherwise, default property is "a" (for `name` and `email`)
    try {
      const statement = SQL`SELECT item_id AS id, name, type, color, taste, date, image, author_id FROM items WHERE ${orderProperty} > ${startingId}`;
      if (author_id) {
        statement.append(SQL` AND author_id = ${author_id}`);
      }
      statement.append(
        desc
          ? SQL` ORDER BY ${orderProperty} DESC`
          : SQL` ORDER BY ${orderProperty} ASC`
      );
      statement.append(SQL` LIMIT ${limit || 100}`);
      const rows = await db.all(statement);
      return rows;
    } catch (e) {
      throw new Error(`couldn't retrieve items: ` + e.message);
    }
  };

  const createUserIfNotExists = async props => {
    const { auth0_sub, nickname } = props;
    const answer = await db.get(
      SQL`SELECT user_id FROM users WHERE auth0_sub = ${auth0_sub}`
    );
    if (!answer) {
      await createUser(props)
      return {...props, firstTime:true } // if the user didn't exist, make that clear somehow
    }
    return props;
  };

  /**
   * Creates a user
   * @param {Object} props an object containing the properties `auth0_sub` and `nickname`.  
   */
  const createUser = async props => {
    const { auth0_sub, nickname } = props;
    const result = await db.run(SQL`INSERT INTO users (auth0_sub, nickname) VALUES (${auth0_sub},${nickname});`);
    return result.stmt.lastID;
  }

  
  const controller = {
    createUserIfNotExists,
    createUser,
    createItem,
    deleteItem,
    updateItem,
    getItem,
    getItemsList
  }

  return controller
}


export default initializeDatabase
